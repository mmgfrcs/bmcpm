const fsUtils = require("./fs");
const chalk = require("chalk");
const hooman = require('hooman');
const ProgressBar = require("progress");
const contentDisp = require("content-disposition");
const path = require('path');
const urlNode = require('url');
const ora = require('ora');
const srcUtil = require('./util');

module.exports = async function(args) {

    let instPluginList = JSON.parse(await fsUtils.openInstalledPluginList());
    let pluginNames = args._.slice(1,args._.length);
    let exp = args.experimental || false;
    
    if(pluginNames.length == 0) {
        //Build plugin list from the provided name
        if(instPluginList.length == 0) throw new Error("No plugins to install");

        pluginNames = instPluginList.map(val => val.name);
    }
    try {
        
        for (let plug of pluginNames) {
            let plugToInst = await findPluginToInstall(plug); //Finds plugin to install
            
            if(plugToInst !== undefined) {
                let mode = plugToInst.file.type == "external" ? "spigot" : "spiget"; //How to install this plugin?
                let pluginOnList = instPluginList.find(x=>x.name == plugToInst.name); //Is this plugin installed according to plugins.json?
                let ver = pluginOnList !== undefined ? pluginOnList.version : plugToInst.version.id; //If yes, the version to install is the one written on plugins.json
                
                console.log(chalk.yellow(`Downloading ${plugToInst.name} version ${ver} on ${mode}`)); //Tell user
                //Begin download and get its installed plugin details object to be saved to file
                let instPluginDetails = await beginDownloadFile(mode, {
                    "plugin": plugToInst, 
                    "installedPlugin": pluginOnList
                }, exp);

                //Replace if it already exists, else add new
                if(instPluginList.find(x=>x.name == plugToInst.name) !== undefined) 
                    instPluginList[instPluginList.indexOf({name: plugToInst.name})] = instPluginDetails;
                else instPluginList.push(instPluginDetails);
            
            }
            else throw new Error(`Package ${plug} not found`);
        }
        await fsUtils.writeInstalledPluginList(JSON.stringify(instPluginList)); //Write installed plugins definition to file
    } 
    catch(err) {
        await fsUtils.writeInstalledPluginList(JSON.stringify(instPluginList));
        throw err;
    }
};

async function findPluginToInstall(name) {
    let pkg = JSON.parse(await fsUtils.openAvailablePluginList());
    return pkg.find(val => val.name == name) || pkg.find(val => {val.name.includes(name)});
}

/**
 * @typedef InstalledPluginDef
 * @property {string} name The name of the installed plugin
 * @property {string} version The version of the installed plugin
 * @property {string} downloadUrl The URL to download this specific version from
 * @property {string} file The file name of the installed plugin
 */

/**
 * @typedef DownloadProperties 
 * @property {Object} plugin
 * @property {InstalledPluginDef} installedPlugin
 */

/**
 * Download a plugin
 * @param {"spigot"|"spiget"} mode 
 * @param {DownloadProperties} properties
 * @param {Array} pluginList
 * @returns {Promise<InstalledPluginDef>}
 */
async function beginDownloadFile(mode, properties, exp) {

    let plugDetails = {};
    if(properties.installedPlugin !== undefined && properties.installedPlugin.downloadUrl !== undefined && !exp) {
        console.log(chalk.yellow("Trying to direct download " + properties.installedPlugin.name));
        plugDetails = await downloadFile(properties.installedPlugin.downloadUrl);
    }
    else plugDetails = await downloadFile(getDownUrl(exp ? "spigot" : mode, properties.plugin.file.url, properties.plugin.id));

    plugDetails.name = properties.plugin.name;
    plugDetails.version = properties.plugin.version.id;
    return plugDetails;
}

/**
 * 
 * @param {"spigot"|"spiget"} mode 
 * @param {String} url
 * @param {number} resId
 */
function getDownUrl(mode, url, resId) {
    if(mode == "spigot") return `https://spigotmc.org/${url}`;
    else return `https://api.spiget.org/v2/resources/${resId}/download`;
}

function getFileName(headers, url) {
    if(headers !== undefined) return contentDisp.parse(headers).parameters.filename;
    else {
        let uri = urlNode.parse(url);
        return path.basename(uri.path);
    }
}

/**
 * Actually downloads the file
 * @param {string} downUrl 
 * @returns {Promise<InstalledPluginDef>}
 */
function downloadFile(downUrl) {
    let promise = Promise.resolve();
    
    if(downUrl.includes("spigotmc.org")) {
        console.log(chalk.yellow("Waiting for response. This is going to take a while."));
        console.log(chalk.yellow("Note that this feature is EXPERIMENTAL."));
        promise = hooman("https://www.spigotmc.org");
    }

    return promise.then(() => {
        console.log(chalk.yellowBright(`${downUrl.includes("spigotmc.org") ? "Connection to spigotmc.org established. " : ""}Sending pre-flight request`));
        return hooman.head(downUrl).then(res=> {
            let fileName = getFileName(res.headers["content-disposition"], res.url);
            let file = fsUtils.createWriteStream(path.join("plugins", fileName));
            return new Promise((resolve, reject) => {
                let spinner = ora("Downloading...");
                let size = 0;
                let fileSize = res.headers["content-length"];
                let progressbar;
                
                if(fileSize === undefined) spinner.start();
                else progressbar = new ProgressBar(`${chalk.green("Downloading")} [:bar] :progCurr/:progTotal (:percent) :etas`, {total: parseInt(fileSize), width: 70});

                hooman.stream(downUrl).on("end", () => {
                    if(fileSize !== undefined)
                        console.log(chalk.green("Download finished"));
                    else spinner.succeed("Download complete");
                    resolve({"downloadUrl": res.url, "file": fileName});
                }).on("data", chunk => {
                    size += chunk.length;
                    if(fileSize !== undefined) progressbar.tick(chunk.length, {progCurr: srcUtil.formatNumber(size, 2), progTotal: srcUtil.formatNumber(fileSize, 2)});
                    else spinner.text = `Downloading... ${srcUtil.formatNumber(size, 2)} downloaded`;
                    
                }).on("error", err=>{
                    if(fileSize === undefined) spinner.fail("Download failed, see error below");
                    reject(err);
                }).pipe(file);
            });
        });

    });
}