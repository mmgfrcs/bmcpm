const fs = require("fs");
const path = require("path");
const chalk = require("chalk");
const paths = require("path");

const packListFile = "packs.json";
const packListDir = "bmcpm";
const pluginListFile = "plugins.json";
const pluginListDir = "./";

try{
    if(!fs.existsSync(packListDir))
        fs.mkdirSync(packListDir, "766");
    if(!fs.existsSync(pluginListDir))
        fs.mkdirSync(pluginListDir, "766");
}
catch(err) {
    if(err.code == 'EACCESS') console.error(chalk.white.bgRed("Error").bgBlack(" EACCESS: Insufficient permission"));
    else if(err.code == 'EPERM') console.error(chalk.white.bgRed("Error").bgBlack(" EPERM: Operation not permitted"));
    else console.error(err);
}

/**
 * @returns {Promise<string>}
 */
function openAvailablePluginList() {
    return new Promise((res, rej) => {
        fs.readFile(path.join(packListDir, packListFile), "utf-8", (err, data) => {
            if(err) rej(err);
            else res(data);
        });
    });
}

function writeAvailablePluginList(data) {
    return new Promise((res, rej) => {
        fs.writeFile(path.join(packListDir, packListFile), data, (err) => {
            if(err) rej(err);
            else res();
        });
    });
}

/**
 * @returns {Promise<string>}
 */
function openInstalledPluginList() {
    return new Promise((res, rej) => {
        if(!fs.existsSync(path.join(pluginListDir, pluginListFile))) return res("[]");
        fs.readFile(path.join(pluginListDir, pluginListFile), "utf-8", (err, data) => {
            if(err) rej(err);
            else res(data);
        });
    });
}

function writeInstalledPluginList(data) {
    return new Promise((res, rej) => {
        fs.writeFile(path.join(pluginListDir, pluginListFile), data, (err) => {
            if(err) rej(err);
            else res();
        });
    });
}

function createWriteStream(path) {
    if(!fs.existsSync(paths.dirname(path))) fs.mkdirSync(paths.dirname(path), {recursive: true});
    return fs.createWriteStream(path, {highWaterMark: Math.pow(2,16)});
}

function writeFile(path, data) {
    return new Promise((res, rej) => {
        if(!fs.existsSync(paths.dirname(path))) fs.mkdirSync(paths.dirname(path), {recursive: true});
        fs.writeFile(path, data, (err) => {
            if(err) rej(err);
            else res();
        });
    });
}

module.exports = {
    openAvailablePluginList, writeAvailablePluginList, openInstalledPluginList, writeInstalledPluginList, createWriteStream, writeFile
};
