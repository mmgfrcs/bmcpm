const fs = require("./fs");
const chalk = require("chalk");
const camelCase = require('camelcase');
const ProgressBar = require('progress');
const Bottleneck = require('bottleneck').default;
const Validator = require("jsonschema").Validator;
const got = require("got").default;
const valid = new Validator();

const limiter = new Bottleneck({
    maxConcurrent: 10,
    minTime: 50
});

module.exports = {run: async function(_args) {
    console.log(chalk.yellow("Updating package list"));
    let packs = [];
    let itemAmt = await getResourceAmount();
    let pages = Math.ceil(itemAmt/1000);
    let requests = [];
    let progressbar = new ProgressBar("Loading Packages :bar :current/:total (:percent) :etas - :start...:end", pages);

    let schema = {
        "type": "object",
        "properties": {
            "name": {type: "string", required: true},
            "version": {
                required: true,
                type: "object", 
                properties: { "id": {type: "number", required: true} }
            },
            "file": {
                "type": "object",
                required: true,
                properties: {
                    "type": {type: "string", required: true},
                    "size": {type: "number", required: true},
                    "sizeUnit": {type: "string", required: true},
                    "url": {type: "string", required: true}
                }
            }
        }
    };

    for(let i = 0 ; i < pages; i++) {
        requests.push(limiter.schedule(() => startFinderRequest(i)).then(async (req) => {
            let arr = req.body;
            
            if(req.status >= 400) throw {code: "E", message: req.statusText};

            arr = arr.filter(val => valid.validate(val, schema).valid);

            for(let i = 0; i<arr.length;i++) 
                arr[i].name = renamePlugin(arr[i]);
            
            progressbar.tick({start: arr[0] ? arr[0].name : "-", end: arr[arr.length-1] ? arr[arr.length-1].name : "-"});
            packs = packs.concat(arr);

        }));
    }
    
    await Promise.all(requests);

    console.log(chalk.green("Loading complete."));
    console.log(chalk.yellow("Removing duplicate packages..."));
    packs = removeDupes(packs);
    console.log(chalk.yellow("Writing to file..."));
    await fs.writeAvailablePluginList(JSON.stringify(packs));
    
    console.log(chalk.green(`Updated ${packs.length} packages`));
    
}, startFinderRequest, getResourceAmount, getFinderRequestPath, renamePlugin, removeDupes};

function startFinderRequest(page) {
    return got.get(getFinderRequestPath(page), {headers: {"User-Agent": "bmcpm"}, responseType: "json"});
}

/**
 * @returns {Promise<Number>}
 */
function getResourceAmount() {
    return got.get("https://api.spiget.org/v2/status", {headers: {"User-Agent": "bmcpm"}, responseType: "json"}).then(res=> res.body.stats.resources);
}

function getFinderRequestPath(page) {
    return "https://api.spiget.org/v2/resources/free?size=1000&page=" + page + "&sort=id&fields=id%2Cname%2Cfile%2Cversion";
}

function renamePlugin(plugin) {
    //Remove all parenthesis and brackets
    let escapeName = plugin.name.replace(/ *[([{][^)\]}]*[)\]}] */g, "");
    //Strip words after these symbols: dash (-), pipe (|)
    escapeName = escapeName.replace(/[|-].*$/g, "");
    //Trim spaces
    escapeName = escapeName.trim();
    //Remove special characters
    escapeName = escapeName.replace(/[^a-zA-Z0-9 ]/g, "");
    //Camelcase
    escapeName = camelCase(escapeName);
    //Write back
    return escapeName;
}

function removeDupes(arr) {
    return Array.from(new Set(arr.map(a => a.name))).map(name => {
        return arr.find(a => a.name === name);
        
    });
}