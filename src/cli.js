const chalk = require('chalk');
const help = require('./help');
const update = require('./update');
const upgrade = require('./upgrade');
const install = require('./install');
const list = require("./list");

module.exports = async function(args) {
    let cmd = args._[0];

    if(args.h || args["?"] || args.help || args._.length == 0) help();

    try {
        switch(cmd) {
            case "list": {
                await list(args);
                break;
            }
            case "update": {
                await update.run(args);
                break;
            }
    
            case "upgrade": {
                await upgrade(args);
                break;
            }
    
            case "install": {
                await install(args);
                break;
            }
        }
    }
    catch(err) {
        
        console.error(`${chalk.bgRed("Error")} ${err.message}`);
    }
};