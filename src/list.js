const fs = require("./fs");
const chalk = require("chalk");
const inquirer = require("inquirer");

module.exports = async function(args) {
    let pkg = JSON.parse(await fs.openAvailablePluginList());
    let findStr = args._[1];
    if(findStr !== undefined) pkg = find(pkg, findStr);

    let entriesPerPage = 100;
    let maxPage = Math.ceil(pkg.length / entriesPerPage);
    let page = 0;

    while(page < maxPage) {
        console.log(chalk.blue(`Package List: Page ${page + 1} of ${maxPage} (${pkg.length} packages)`));
        for(let idx = entriesPerPage * page; idx < Math.min(pkg.length, entriesPerPage*(page+1));idx++) 
            console.log(`    ${(pkg[idx].file.type == "external" ? chalk.yellow(pkg[idx].name) : chalk.cyanBright(pkg[idx].name))}:${pkg[idx].version.id}`);
        
        if(page + 1 < maxPage) {
            let cont = await inquirer.prompt({
                type: "list", 
                name: "continue", 
                message: `Showing ${entriesPerPage} packages at one time. Page ${page + 1} of ${maxPage} (${pkg.length} packages)`, 
                choices: [{name: "Continue", value: true}, {name: "Exit", value: false}]
            });
            if(!cont.continue) break;
            else page++;
        }
        else break;
    }
};

/**
 * Find package by name
 * @param {Array} arr 
 */
function find(arr, name) {
    return arr.filter(val=> {
        return val.name.includes(name);
    });
}