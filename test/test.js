const srcUpdate = require("../src/update");
const srcUtil = require("../src/util");
const fs = require("../src/fs");
const chai = require("chai");
const expect = chai.expect;

chai.use(require("chai-fs"));

describe("Utility functions", function() {
    it("Can format number", function() {
        expect(srcUtil.formatNumber(111, 0)).to.be.equal("111");
        expect(srcUtil.formatNumber(111111, 0)).to.be.equal("111K");
        expect(srcUtil.formatNumber(111111111, 0)).to.be.equal("111M");
        expect(srcUtil.formatNumber(111111111111, 0)).to.be.equal("111G");
        expect(srcUtil.formatNumber(111111111111, 1)).to.be.equal("111.1G");
        expect(srcUtil.formatNumber(11111111111, 2)).to.be.equal("11.11G");
    });
    it("Can capitalize first letter", function() {
        expect(srcUtil.capitalizeFirstLetter("ana")).to.be.equal("Ana");
        expect(srcUtil.capitalizeFirstLetter("ana ana")).to.be.equal("Ana ana");
    });
});

describe("bmcpm update", function() {
    it("Can get resource amount", function() {
        return srcUpdate.getResourceAmount().then(n => {
            expect(n).to.be.greaterThan(0);
        });
    });
    it("Can get finder request path", function() {
        expect(srcUpdate.getFinderRequestPath(1)).to.be.eq("https://api.spiget.org/v2/resources/free?size=1000&page=1&sort=id&fields=id%2Cname%2Cfile%2Cversion");
        expect(srcUpdate.getFinderRequestPath(-1)).to.be.eq("https://api.spiget.org/v2/resources/free?size=1000&page=-1&sort=id&fields=id%2Cname%2Cfile%2Cversion");
    });
    it("Can rename plugin", function() {
        //Simple name
        expect(srcUpdate.renamePlugin({name: "Zombie Survival"})).to.be.eq("zombieSurvival");
        //Name with some symbols
        expect(srcUpdate.renamePlugin({name: "StaffUtils [Fully Configurable & Customizable]"})).to.be.eq("staffUtils");
        //Name with dash
        expect(srcUpdate.renamePlugin({name: "CMOTD - Center your MOTD "})).to.be.eq("cmotd");
        //Wondrous name
        expect(srcUpdate.renamePlugin({name: "✨ HoloChat ✨ | New hologram chat ✅Multiple Versions ✅[1.8-1.15]"})).to.be.eq("holoChat");
    });
    it("Can remove duplicates", function() {
        expect(srcUpdate.removeDupes([{name: "A"}, {name: "A"},{name: "B"}])).to.be.an.instanceOf(Array).with.lengthOf(2);
        expect(srcUpdate.removeDupes([{name: "A"},{name: "B"}])).to.be.an.instanceOf(Array).with.lengthOf(2);
    });
    it("Can do it all", function() {
        this.timeout(15000);
        return srcUpdate.run().then(() => fs.openAvailablePluginList()).then(pkgList => {
            let pkgs = JSON.parse(pkgList);
            expect(pkgs).to.be.an.instanceOf(Array);
            expect(pkgs).to.have.length.greaterThan(0);
        });
    });
});