# bmcpm
Bukkit Minecraft Plugin Manager

Install bukkit plugins for your server right from the command line! It support plugins on spigotmc.org via Spiget API, with more added in the future. 

## Install
You can install this package through npm:

```npm install -g bmcpm```

You can also use this package without installing through npx:

```npx bmcpm```

There's also a binary that you can download on the [Releases](https://gitlab.com/mmgfrcs/bmcpm/-/releases) page

## Usage
bmcpm commands are inspired by apt, so some of the commands will be familiar. Because of this, each of ```bmcpm``` commands generally falls in this format:

```bmcpm <command> <options> <arguments>```

For first-time use, run the commands below **in order**.

### ```bmcpm update```
> ```bmcpm update```
Update the list of plugins. This will call the API, take the name of all plugins and change it so it can be typed on the command line, since a lot of plugins have spaces, emojis and symbols in their name that would not be convenient to type.

The result of this command can be seen in ```bmcpm/packs.json```

### ```bmcpm list```
> ```bmcpm list [pluginNameToSearch]```

Outputs a list of plugin names that you can install using ```bmcpm```. You can also search for plugins by typing its name after the command.

For example, to search for plugins whose name contains the word "dyn": ```bmcpm list dyn```

Some plugins on the list will be colored yellow. Those plugins are external plugins that the API cannot install, therefore support for installing those plugins are **Experimental**. See below for more explanation on this.

### ```bmcpm install```
> ```bmcpm install [--experimental] [pluginName...]```

Installs plugins. This command assumes that the working directory is the server's directory (where you'll usually put the server jar)

When no arguments are given, this command install all plugins as stated in ```plugins.json``` in the working directory. Due to API limitations, this command will always install the latest version.

When one or more arguments are given, for each argument, the command finds its respective plugin and installs it.

The command will use the API to find the plugin file. However, since some plugins are not hosted on spigotmc.org and uses external download (this is marked on ```bmcpm list``` in yellow), the command will fall back to installing it directly from spigotmc.org. Supplying the ```--experimental``` flag will cause the command to install all the plugins by direct download, therefore bypassing the API.

Please note that direct download from spigotmc.org is very much an **experimental** feature, since there's no easy way of doing so and the website itself does not have an API to facilitate such feature.
